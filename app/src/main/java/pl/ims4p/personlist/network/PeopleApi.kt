package pl.ims4p.personlist.network

import io.reactivex.Observable
import pl.ims4p.personlist.model.Person
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface PeopleApi {

    @GET("5db2e026350000a91af5527c")
    fun getPeople(): Observable<List<Person>>

}
