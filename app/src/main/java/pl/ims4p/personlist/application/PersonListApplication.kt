package pl.ims4p.personlist.application

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.ims4p.personlist.BuildConfig
import pl.ims4p.personlist.di.appModule
import pl.ims4p.personlist.di.networkModule
import pl.ims4p.personlist.di.roomModule
import timber.log.Timber



class PersonListApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PersonListApplication)

            modules(listOf(
                appModule,
                networkModule,
                roomModule
            ))
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(Timber.asTree())
        }
    }

}