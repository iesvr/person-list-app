package pl.ims4p.personlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import pl.ims4p.personlist.databinding.PersonListActivityBinding
import pl.ims4p.personlist.ui.people.PeopleFragment

class PersonListActivity : AppCompatActivity() {

    private lateinit var binding: PersonListActivityBinding
    private lateinit var viewModel: PersonListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.person_list_activity)
        viewModel = ViewModelProviders.of(this).get(PersonListViewModel::class.java)
        binding.viewModel = viewModel

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PeopleFragment.newInstance())
                .commitNow()
        }
    }

}
