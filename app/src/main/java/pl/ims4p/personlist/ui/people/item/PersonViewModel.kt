package pl.ims4p.personlist.ui.people.item

import android.content.Context
import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.ims4p.personlist.R
import pl.ims4p.personlist.model.Person
import pl.ims4p.personlist.util.getCurrentYear
import pl.ims4p.personlist.util.isAdultAge

class PersonViewModel: KoinComponent {

    val context: Context by inject()

    private val personName = MutableLiveData<String>()
    private val personYearOfBirth = MutableLiveData<Int>()
    private val personStatus = MutableLiveData<Boolean>()

    fun bind(person: Person) {
        personName.value = person.name
        personYearOfBirth.value = person.yearOfBirth
        personStatus.value = person.student
    }

    fun getPersonName(): MutableLiveData<String> = personName

    fun getPersonYearOfBirth(): MutableLiveData<Int> = personYearOfBirth

    fun getPersonYearOfBirthString(): MutableLiveData<String> = MutableLiveData<String>(getPersonYearOfBirth().value?.toString())

    fun getPersonStatus(): MutableLiveData<Boolean> = personStatus

    fun getAdultMessage(): MutableLiveData<String> {
        val adultStatus = if (isAdultAge(personYearOfBirth.value ?: getCurrentYear()) == true)
            context.resources.getString(R.string.common_yes) else context.resources.getString(R.string.common_no)
        val response = String.format("%s %s", context.resources.getString(R.string.person_item_adult), adultStatus)
        return MutableLiveData<String>(response)
    }

    fun getStatusMessage(): MutableLiveData<String> {
        val status = if (personStatus.value ?: false)
            context.resources.getString(R.string.person_item_student) else context.resources.getString(R.string.person_item_not_student)
        val response = String.format("%s %s", context.resources.getString(R.string.person_item_status), status)
        return MutableLiveData<String>(response)
    }

}
