package pl.ims4p.personlist.ui.people.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pl.ims4p.personlist.R
import pl.ims4p.personlist.databinding.ViewPeopleListItemBinding
import pl.ims4p.personlist.model.Person
import pl.ims4p.personlist.ui.people.item.PersonViewModel

class PeopleListAdapter: RecyclerView.Adapter<PeopleListAdapter.ViewHolder>() {

    private lateinit var peopleList: List<Person>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleListAdapter.ViewHolder {
        val binding: ViewPeopleListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.view_people_list_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PeopleListAdapter.ViewHolder, position: Int) {
        holder.bind(peopleList[position])
    }

    override fun getItemCount(): Int =
        if (::peopleList.isInitialized) peopleList.size else 0

    fun updatePeopleList(peopleList: List<Person>){
        this.peopleList = peopleList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ViewPeopleListItemBinding): RecyclerView.ViewHolder(binding.root) {
        private val viewModel = PersonViewModel()

        fun bind(person: Person) {
            viewModel.bind(person)
            binding.viewModel = viewModel
        }
    }
}
