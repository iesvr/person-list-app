package pl.ims4p.personlist.ui.people

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import pl.ims4p.personlist.R
import pl.ims4p.personlist.databinding.PeopleListFragmentBinding
import pl.ims4p.personlist.ui.people.adapter.PeopleListAdapter

class PeopleFragment : Fragment() {

    private lateinit var viewModel: PeopleViewModel
    private lateinit var binding: PeopleListFragmentBinding

    private var errorSnackbar: Snackbar? = null

    val peopleListAdapter: PeopleListAdapter = PeopleListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate<PeopleListFragmentBinding>(inflater, R.layout.people_list_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PeopleViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })

        binding.viewModel = viewModel

        viewModel.getPeople().observe(this, Observer { data ->
            data.let {
                peopleListAdapter.apply {
                    updatePeopleList(ArrayList(it))
                }
            }
        })

        viewModel.loadPeople()

        binding.peopleList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = peopleListAdapter
        }
    }

    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.common_retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    companion object {
        fun newInstance() = PeopleFragment()
    }

}
