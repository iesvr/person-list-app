package pl.ims4p.personlist.ui.people

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.ims4p.personlist.R
import pl.ims4p.personlist.di.createApi
import pl.ims4p.personlist.model.Person
import pl.ims4p.personlist.model.dao.PersonDao
import pl.ims4p.personlist.network.PeopleApi
import retrofit2.Retrofit
import timber.log.Timber

class PeopleViewModel : ViewModel(), KoinComponent {

    val retrofit: Retrofit by inject()
    val peopleApi: PeopleApi = createApi<PeopleApi>(retrofit)
    val personDao: PersonDao by inject()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPeople() }

    private val people: MutableLiveData<List<Person>> = MutableLiveData()

    private var disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun getPeople(): LiveData<List<Person>> = people as LiveData<List<Person>>

    fun loadPeople() {
        disposable.add(
            peopleApi.getPeople()
                .concatMap { personList ->
                    personDao.insertAll(*personList.toTypedArray())
                    Observable.just(personDao.all)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePeopleListStart() }
                .doOnTerminate { onRetrievePeopleListFinish() }
                .subscribe(
                    { result -> onRetrievePeopleListSuccess(result) },
                    { error -> onRetrievePeopleListError(error) }
                )
        )
    }

    private fun onRetrievePeopleListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePeopleListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePeopleListSuccess(peopleList: List<Person>) {
        loadingVisibility.value = View.GONE
        people.value = peopleList
    }

    private fun onRetrievePeopleListError(error: Throwable) {
        loadingVisibility.value = View.GONE
        Timber.e("EXCEPTION: ${error.message}")

        disposable.add(
            Observable.fromCallable { personDao.all }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    onRetrievePeopleListSuccess(it)
                }
        )

        errorMessage.value = R.string.common_post_error
    }

}
