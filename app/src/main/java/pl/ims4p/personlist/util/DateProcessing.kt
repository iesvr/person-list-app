package pl.ims4p.personlist.util

import java.util.*

fun getCurrentYear(): Int {
    return Calendar.getInstance().get(Calendar.YEAR)
}

fun isAdultAge(year: Int): Boolean
        = if ((getCurrentYear() - year) >= 18) true else false
