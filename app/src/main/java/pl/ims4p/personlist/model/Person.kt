package pl.ims4p.personlist.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

@Entity(primaryKeys = arrayOf("name", "yearOfBirth", "student"))
data class Person(

    @NotNull
    var name: String,

    @NotNull
    @SerializedName("year_of_birth")
    var yearOfBirth: Int,

    @NotNull
    var student: Boolean
)
