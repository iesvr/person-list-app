package pl.ims4p.personlist.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.ims4p.personlist.model.Person
import pl.ims4p.personlist.model.dao.PersonDao

@Database(entities = arrayOf(Person::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun personDao(): PersonDao

}
