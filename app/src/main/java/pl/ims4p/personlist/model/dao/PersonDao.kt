package pl.ims4p.personlist.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.ims4p.personlist.model.Person

@Dao
interface PersonDao {

    @get:Query("SELECT * FROM person ORDER BY name ASC")
    val all: List<Person>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(vararg posts: Person)

}
