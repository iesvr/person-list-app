package pl.ims4p.personlist.di

import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import pl.ims4p.personlist.model.db.AppDatabase

val roomModule = module {

    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "people-db")
            .build()
    }

    single { get<AppDatabase>().personDao() }

}
